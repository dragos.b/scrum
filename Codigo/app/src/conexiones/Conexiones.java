package conexiones;

import java.sql.*;

public class Conexiones {
    // Metodo de conexión
    public static Connection conectarDB ( String url, String user, String pass ) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection ( url, user, pass );
            System.out.println ( "Conexion a bbdd hecha !" );

        } catch ( SQLException e ) {
            e.printStackTrace ();
        }
        return conn;
    }

    // Consulta el total de clases y  devuelve el resultado
    public static ResultSet consultaClase ( Connection con ) {
        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement ( "SELECT * FROM clase" );
            ResultSet rs = stmt.executeQuery ();
            return rs;
        } catch ( SQLException e ) {
            e.printStackTrace ();
            return null;
        }

    }

    public static ResultSet consultaClaseAlumno ( Connection con ) {
        PreparedStatement stmt;
        try {
            stmt = con.prepareStatement ( "SELECT nombre_clase FROM alumno GROUP BY nombre_clase" );
            ResultSet rs = stmt.executeQuery ();
            return rs;
        } catch ( SQLException e ) {
            e.printStackTrace ();
            return null;
        }

    }

    // Consulta el total de alumnos de una clase y devuelve el resultado
    public static ResultSet consultaTotalAlumno ( Connection con, String clase ) {
        try {
            PreparedStatement stmt = con
                    .prepareStatement ( "SELECT COUNT(*) AS 'tot' FROM alumno WHERE nombre_clase = '" + clase + "'" );
            ResultSet         rs   = stmt.executeQuery ();
            return rs;
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }
        return null;

    }

    // Consulta todos los alumnos de una clase, para guardar los ID's
    public static ResultSet consultaAlumno ( Connection con, String clase ) {

        try {
            PreparedStatement stmt = con
                    .prepareStatement ( "SELECT * FROM alumno WHERE nombre_clase = '" + clase + "'" );
            ResultSet         rs   = stmt.executeQuery ();
            return rs;
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }
        return null;
    }

    public static ResultSet consultaAlumnoId ( Connection con, int id_al ) {
        try {
            PreparedStatement stmt = con.prepareStatement ( "SELECT * FROM alumno WHERE id_alumno = " + id_al );
            ResultSet         rs   = stmt.executeQuery ();
            return rs;
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }
        return null;
    }

    // Aumenta en 1 'n_correctas' pasando un 'id_alumno'
    public static void puntoPositivo ( Connection con, int id ) {
        int               puntuacion;
        PreparedStatement select, update;
        String            columna = "n_correctas";
        try {
            // Consulta n_correctas de id_alumno y se guarda en puntuación
            select = con.prepareStatement ( "SELECT " + columna + " FROM puntuacion WHERE id_alumno = " + id );
            ResultSet rs = select.executeQuery ();
            rs.next ();
            // Suma 1 a la puntuación obtenida y ejecuta un UPDATE.
            puntuacion = 1 + rs.getInt ( columna );
            update     = con.prepareStatement (
                    "UPDATE puntuacion SET " + columna + " = " + puntuacion + " WHERE id_alumno = " + id );
            update.executeUpdate ();
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }

    }

    // Aumenta en 1 'n_incorrectas' pasado el 'id_alumno'
    public static void puntoNegativo ( Connection con, int id ) {
        int               puntuacion;
        PreparedStatement select, update;
        String            columna = "n_incorrectas";
        try {
            // Consulta n_incorrectas de id_alumno y se guarda en puntuación
            select = con.prepareStatement ( "SELECT " + columna + " FROM puntuacion WHERE id_alumno = " + id );
            ResultSet rs = select.executeQuery ();
            rs.next ();
            // Suma 1 a la puntuación obtenida y ejecuta un UPDATE.
            puntuacion = 1 + rs.getInt ( columna );
            update     = con.prepareStatement (
                    "UPDATE puntuacion SET " + columna + " = " + puntuacion + " WHERE id_alumno = " + id );
            update.executeUpdate ();
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }
    }

    // Crear un alumno
    public static void crearAlumno ( Connection con, String nombre, String ap1, String ap2, String clase ) {

        PreparedStatement insertar;
        PreparedStatement consultar;
        try {
            insertar = con.prepareStatement ( "INSERT INTO alumno (nombre_clase, nombre, apellido1, apellido2) VALUES('"
                    + clase + "','" + nombre + "','" + ap1 + "','" + ap2 + "');" );
            insertar.executeUpdate ();
            consultar = con.prepareStatement ( "SELECT id_alumno FROM alumno WHERE nombre_clase ='" + clase
                    + "' AND nombre = '" + nombre + "' AND apellido1 ='" + ap1 + "'" );
            ResultSet idAl = consultar.executeQuery ();
            idAl.next ();
            insertar = con.prepareStatement (
                    "INSERT INTO puntuacion (id_alumno) VALUES (" + idAl.getInt ( "id_alumno" ) + ")" );
            insertar.executeUpdate ();
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }
    }

    // Crear una clase
    public static void crearClase ( Connection con, String nombre ) {
        PreparedStatement insertar;
        try {
            insertar = con.prepareStatement ( "INSERT INTO clase (nombre_clase) VALUES ('" + nombre + "');" );
            insertar.executeUpdate ();
        } catch ( SQLException e ) {
            e.printStackTrace ();
        }
    }
}