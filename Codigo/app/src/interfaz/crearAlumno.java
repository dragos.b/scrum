package interfaz;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import conexiones.Conexiones;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Cursor;
import javax.swing.JComboBox;

public class crearAlumno extends JFrame {

    private JPanel contentPane;
    private final JLabel lblRandomapp = new JLabel ( "Añadir Alumno" );
    private JTextField apellido1;
    private JTextField apellido2;

    String urlConexion = "jdbc:mysql://localhost:3306/scrum?serverTimezone=UTC";
    String usuario = "admin";
    String password = "admin123";
    Connection con = Conexiones.conectarDB ( urlConexion, usuario, password );

    public crearAlumno () {
        setDefaultCloseOperation ( JFrame.HIDE_ON_CLOSE );
        setBounds ( 100, 100, 571, 490 );
        setTitle ( "Random App -  Añadir Alumno" );
        setResizable ( false );
        contentPane = new JPanel ();
        setContentPane ( contentPane );
        contentPane.setLayout ( null );

        // Primer Panel (Rojo)
        JPanel panel = new JPanel ();
        panel.setBackground ( new Color ( 255, 0, 0 ) );
        panel.setBounds ( 0, 0, 571, 116 );
        contentPane.add ( panel );
        panel.setLayout ( null );
        lblRandomapp.setBounds ( 0, 12, 571, 116 );
        lblRandomapp.setFont ( new Font ( "SansSerif", Font.BOLD, 49 ) );
        lblRandomapp.setHorizontalAlignment ( SwingConstants.CENTER );
        lblRandomapp.setForeground ( Color.WHITE );
        panel.add ( lblRandomapp );

        // Segundo Panel el blanco que contiene botones
        JPanel panel_1 = new JPanel ();
        panel_1.setBackground ( new Color ( 255, 255, 255 ) );
        panel_1.setBounds ( 0, 118, 571, 341 );
        contentPane.add ( panel_1 );
        panel_1.setLayout ( null );

        // Label e Input nombre
        JLabel lblNombre = new JLabel ( "Nombre:" );
        lblNombre.setBounds ( 12, 61, 70, 15 );
        lblNombre.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( lblNombre );

        JTextField nombre = new JTextField ( 35 );
        nombre.setToolTipText ( "Escribe el nombre del alumno" );
        nombre.setBounds ( 12, 88, 156, 32 );
        nombre.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( nombre );

        // Label e Input apellido1
        JLabel lblApellido1 = new JLabel ( "Primer Apellido:" );
        lblApellido1.setBounds ( 204, 61, 130, 15 );
        lblApellido1.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( lblApellido1 );

        JTextField apellido1 = new JTextField ( 35 );
        apellido1.setToolTipText ( "Escribe el primer apellido" );
        apellido1.setBounds ( 207, 88, 156, 32 );
        apellido1.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( apellido1 );

        // Label e Input apellido2
        JLabel lblApellido2 = new JLabel ( "Segundo Apellido:" );
        lblApellido2.setBounds ( 386, 61, 156, 15 );
        lblApellido2.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( lblApellido2 );

        JTextField apellido2 = new JTextField ( 35 );
        apellido2.setToolTipText ( "Escribe el segundo apellido" );
        apellido2.setBounds ( 386, 88, 156, 32 );
        apellido2.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( apellido2 );

        // Seleccionable
        JLabel lblSelecciona = new JLabel ( "Selecciona Clase para el/la alumno/a" );
        lblSelecciona.setBounds ( 142, 143, 274, 31 );
        panel_1.add ( lblSelecciona );

        JComboBox comboBox = new JComboBox ();
        comboBox.setCursor ( Cursor.getPredefinedCursor ( Cursor.HAND_CURSOR ) );
        comboBox.setBackground ( Color.WHITE );
        comboBox.setBounds ( 130, 173, 295, 32 );
        panel_1.add ( comboBox );
        comboBox.addItem ( "Clases..." );
        ResultSet res = Conexiones.consultaClase ( con );
        try {
            while ( res.next () ) {
                try {
                    comboBox.addItem ( res.getString ( "nombre_clase" ) );
                } catch ( SQLException e1 ) {
                    e1.printStackTrace ();
                }
            }
        } catch ( SQLException e1 ) {
            e1.printStackTrace ();
        }

        // Boton Añadir Alumno
        JButton botonCrear = new JButton ( "Añadir Alumno" );
        botonCrear.setCursor ( Cursor.getPredefinedCursor ( Cursor.HAND_CURSOR ) );
        botonCrear.setForeground ( new Color ( 255, 255, 255 ) );
        botonCrear.setBackground ( new Color ( 255, 0, 0 ) );
        botonCrear.setBounds ( 206, 249, 157, 40 );
        botonCrear.setFont ( new Font ( "Sans Serif", Font.BOLD, 14 ) );
        panel_1.add ( botonCrear );

        // Inserta un alumno y deja los campos de texto vacios
        botonCrear.addActionListener ( new ActionListener () {
            public void actionPerformed ( ActionEvent e ) {
                String nombreA = nombre.getText (), apellido1A = apellido1.getText (),
                        apellido2A = apellido2.getText ();

                String claseA = (String)comboBox.getSelectedItem ();
                Conexiones.crearAlumno ( con, nombreA, apellido1A, apellido2A, claseA );
                nombre.setText ( "" );
                apellido1.setText ( "" );
                apellido2.setText ( "" );
            }
        } );
        
          //Boton Atras
        JButton botonAtras = new JButton("Atrás");
        botonAtras.setForeground(Color.WHITE);
        botonAtras.setBounds(450, 304, 92, 25);
        botonAtras.setBackground ( new Color ( 255, 0, 0 ) );
        panel_1.add(botonAtras);
        //Vuelve a la clase Inicio
        botonAtras.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	Inicio insertarInicio = new Inicio ();
            insertarInicio.setVisible ( true );
            insertarInicio.setLocationRelativeTo ( null );
            dispose();
        	}
        });
		

    }
}
