DROP DATABASE IF EXISTS scrum;
CREATE DATABASE scrum;
USE scrum;

CREATE TABLE clase (
    nombre_clase VARCHAR(40),
    CONSTRAINT clase_pk PRIMARY KEY ( nombre_clase )
);

CREATE TABLE alumno (
    id_alumno INT AUTO_INCREMENT,
    nombre_clase VARCHAR(40),
    nombre VARCHAR(80),
    apellido1 VARCHAR(60),
    apellido2 VARCHAR(60),
    CONSTRAINT alumno_pk PRIMARY KEY ( id_alumno ),
    CONSTRAINT alumno_clase FOREIGN KEY ( nombre_clase ) REFERENCES clase ( nombre_clase )
);

CREATE TABLE puntuacion (
    id_alumno INT,
    n_correctas SMALLINT DEFAULT 0,
    n_incorrectas SMALLINT DEFAULT 0,
    porcentaje_acierto INT DEFAULT 0,
    puntuacion INT DEFAULT 0,
    CONSTRAINT puntuacion_pk PRIMARY KEY (id_alumno),
    CONSTRAINT puntuacion_alumno_fk FOREIGN KEY ( id_alumno ) REFERENCES alumno ( id_alumno )
);
